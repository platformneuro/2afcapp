import dash
import dash_auth
import dash_bootstrap_components as dbc
from flask import Flask, send_file
import os

# external_stylesheets = [
#     {
#         "href": "https://fonts.googleapis.com/css2?"

#                 "family=Lato:wght@400;700&display=swap",

#         "rel": "stylesheet",
#     },
# ]

# bootstrap theme
# https://bootswatch.com/lux/
external_stylesheets = [dbc.themes.LUX]

# Data path
with open('dataPath.txt') as f:
    c = f.readlines()
dataPath = c[0].rstrip()

# Server with static folder
server = Flask(__name__)
app = dash.Dash(__name__, server = server, external_stylesheets=external_stylesheets)
app.title = "2AFC viewer"

@server.route('/videos/<path:path>')
# @server.route('C://Users/Quentin Gaucher/code/2afcapp/videos/<path:path>') # Not working, requires relative path
def serve_static(path):
    # root_dir = os.getcwd()
    return send_file(os.path.join(dataPath,'Video',path))

# authorization
# Maybe not needed anymore since the ENS network is shieled from the outside now.
# In any case, it needs to be more secure to be efficient
# VALID_USERNAME_PASSWORD_PAIRS = {
#     '2AFC-viewer': '7FERRETontheroof!*'
# }
# auth = dash_auth.BasicAuth(
#     app,
#     VALID_USERNAME_PASSWORD_PAIRS
# )

# server = app.server
app.config.suppress_callback_exceptions = True
