2AFC ferret application

Dash based application to visualize the results of behavioral 2AFC training.

Setup
Clone to server
git clone https://QNeuron@bitbucket.org/platformneuro/2afcapp.git

cd 2afcapp

Install librairies:
pip install -r requirements.txt

Edit dataPath.txt with correct path to data root

Run server:
python index.py -host [ip] -port [port] -debug[True/False]

python index.py -host '129.199.80.243'


to add to requirements.txt
dash_bootstrap_components==2.0.0
dash_auth==1.4.1

