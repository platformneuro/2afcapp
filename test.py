import os
import dash
import dash_html_components as html
from dash.dependencies import Input, Output

from flask import Flask, Response, send_from_directory

# Video format : h264 codec -> To implement from the cage code (ie transfer_video.py)
# ffmpeg -i Oscypek_session5_211122.avi -vcodec h264 Oscypek_session5_211122.mp4

server = Flask(__name__, static_folder = 'C://Users/Quentin Gaucher/Videos')
app = dash.Dash(__name__, server=server)


app.layout = html.Div(children=[html.Video(id = 'video-player', src='', controls=True),  # or .mp4, or .avi ..etc
html.Button("video-button", id="btn_csv",className="button")])

@server.route('/videos/<path:path>')
# @server.route('C://Users/Quentin Gaucher/code/2afcapp/videos/<path:path>') # Not working, requires relative path
def serve_static(path):
    root_dir = os.getcwd()
    return send_from_directory(os.path.join(root_dir, 'videos'), path)

@app.callback(
    [Output("video-player", "src"),
    Output("btn_csv", "n_clicks")
    ],
    [Input("btn_csv", "n_clicks"),
    ],)

def change_video(nclicks):
    # root_dir = os.getcwd()
    # return ['/videos/testVid.mp4']
    print(nclicks)
    if nclicks > 5:
        p = '/videos/Oscypek_session5_211122.mp4'
    else:
        p = '/videos/testVid.mp4'
    return p, nclicks

# @server.route('/videos/<path:path>')
# def serve_static(path):
#     root_dir = os.getcwd()
#     return send_from_directory(os.path.join(root_dir, 'videos'), path)

app.run_server()