import csv
# from distutils.filelist import findall
from logging import warning
import re
import numpy as np
import pandas as pd
from datetime import datetime
import yaml

def trialDataFrame(allGoodTrials, startingSessionNB=0):
            
    allTrialsDF = pd.DataFrame()
    for trials in allGoodTrials:
        df = pd.DataFrame(trials)
        df['session nb corr'] = startingSessionNB
        allTrialsDF = allTrialsDF.append(df)
        startingSessionNB += 1
    return allTrialsDF

class dataManage(object):
    def __init__(self, path='', hasCorrectionTrials=""):
        self.path = path
        self.sessionNb = int(re.findall('session([0-9]{1,3})', path)[0])
        self.date = datetime.strptime(re.findall('_([0-9]{6})', path)[0], '%d%m%y')
        self = self.readCSV()
        try:
            # Has a parameter header specifying the correction trials status in the data csv files
            self.hasCorrectionTrials = self.parameters["hasCorrectionTrials"]
        except:
            # Depend on input
            if hasCorrectionTrials:
                self.hasCorrectionTrials = 'True-v1'
            else:
                # Default case - no correction trials.
                self.hasCorrectionTrials = 'False-v1'

    def readCSV(self):
        f = csv.reader(open(self.path))

        rews = []
        stims = []
        licks = []
        pokes = []
        parameters = dict()
        parameters_raw = []
        for row in f:
            
            for col in row:
                # Remove ', " and ' ' from text
                col = col.replace("'","").replace('"',"").replace(' ','')

                if len(re.findall("script:", col))>0:
                    t = re.findall("script:(.*)", col)[0]
                    if t!='':
                        parameters["script"] = t
                    rawStr = ','.join(row)
                    try:
                        parameters_raw = re.findall('parameters:(.*)',rawStr)[0].replace('"','')
                        parameters_raw = yaml.load(parameters_raw, Loader=yaml.FullLoader)
                        parameters_raw['machine'] = re.findall('machine:(.*),parameters',rawStr)[0]
                        parameters_raw['script'] = re.findall('script:(.*),machine',rawStr)[0]
                        # parameters_raw = ''.join(c for c in parameters_raw if not c in ('{','}',' ',"'",'"'))
                        # yaml.load(s)
                    except:
                        warning('Could not parse raw parameters')
                        parameters_raw = []
                    

                if len(re.findall("cTrial:", col))>0:
                    # Old way to call correction trials in parameters. Kept for backward compatibility
                    t = re.findall("cTrial:(.*)", col)[0]
                    if t!='':
                        parameters["hasCorrectionTrials"] = t == "True"

                if len(re.findall("correctionTrials:", col))>0:
                    # Good way to call correction trials in parameters.
                    t = re.findall("correctionTrials:(.*)", col)[0]
                    if t!='':
                        parameters["hasCorrectionTrials"] = t == "True"

                if len(re.findall("nCorrectionTrials:", col))>0:
                    t = re.findall("nCorrectionTrials:(.*)", col)[0]
                    if t!='':
                        parameters["NCorrectionTrials"] = float(t)

                if len(re.findall("delay:", col))>0:
                    t = re.findall("delay:(.*)", col)[0]
                    if t!='':
                        parameters["delay"] = float(t)

                if len(re.findall("punishment:", col))>0:
                    t = re.findall("punishment:(.*)", col)[0]
                    if t!='':
                        parameters["punishment"] = t == "True"

                if len(re.findall("punishmentDur:", col))>0:
                    t = re.findall("punishmentDur:(.*)", col)[0]
                    if t!='':
                        parameters["punishmentDur"] = float(t)

                if len(re.findall("rewList:", col))>0:
                    t = re.findall("rewList:(.*)", col)[0]
                    if t!='':
                        tmprews = t.split("-")
                        tmprews = [x for x in tmprews if not ("C" in x)] # remove the "C" rewards from list
                        rews = rews + tmprews
                        
                if len(re.findall("sndList:", col))>0:
                    t = re.findall("sndList:(.*)", col)[0]
                    
                    if t!='':
                        stims = stims + t.split("-")
                        
                if len(re.findall("lickList:", col))>0:
                    t = re.findall("lickList:(.*)", col)[0]
                    if t!='':
                        t_sides = ["L" if "L" in i else "R" if "R" in i else "C" for i in t.split("-")]
                        try:
                            t_times = [float(re.findall("([0-9]{1,5}.[0-9]{1,5})[R,L,C]",i)[0]) for i in t.split("-")]
                        except:
                            print('error')
                            print(t)
                        t_ = [[i,j] for i,j in zip(t_times,t_sides)]
                        licks = licks + t_
                        
                if len(re.findall("pokeList:", col))>0:
                    t = re.findall("pokeList:(.*)", col)[0]
                    if t!='':
                        t_sides = ["L" if "L" in i else "R" if "R" in i else "C" for i in t.split("-")]
                        try:
                            t_times = [float(re.findall("([0-9]{1,5}.[0-9]{1,5})[R,L,C]",i)[0]) for i in t.split("-")]
                        except:
                            print(t.split("-"))
                        t_ = [[i,j] for i,j in zip(t_times,t_sides)]
                        pokes = pokes + t_

        rew_sides = [re.findall("[0-9]{1,5}.{0,2}[0-9]{0,5}([A-z]*)",i)[0] for i in rews]
        rew_t = [float(re.findall("([0-9]{1,5}.{0,2}[0-9]{0,5})[A-z]*",i)[0]) for i in rews]
        try:
            stim_type = [int(re.findall("[0-9]{1,5}.{0,2}[0-9]{0,5}([0,1,2])",i)[0]) for i in stims]
        except:
            print(stims)
        stim_t = [float(re.findall("([0-9]{1,5}.{0,2}[0-9]{0,5})[A-z]*",i)[0]) for i in stims]
        # stim_names = [re.findall("[0-9]{1,5}.[0-9]{0,5}([A-z ']*[0-9]*[A-z]*)",i) for i in stims]
        stim_names = [re.findall("[0-9]{1,5}.[0-9]{0,5}([A-z0-9 _']*)",i) for i in stims]
        poke_t = [i[0] for i in pokes]
        lick_t = [i[0] for i in licks]

        rew_sides = np.array(rew_sides)
        rew_t = np.array(rew_t)
        stim_type = np.array(stim_type)
        stim_t = np.array(stim_t)
        lick_t = np.array(lick_t)
        poke_t = np.array(poke_t)

        self.rew_sides = rew_sides
        self.rew_t = rew_t
        self.stim_type = stim_type
        self.stim_t = stim_t
        self.stim_names = stim_names
        self.poke_t = poke_t
        self.lick_t = lick_t
        self.stims = stims
        self.pokes = pokes
        self.licks = licks
        self.parameters = parameters
        self.parameters_raw = parameters_raw

        return self

    def getGoodTrials(self, intervalDur=10, format='dict'):
            # 0 - target - left
        # Lick time and direction
        #licktC = np.array(self.lick_t)[np.where(np.array(self.licks)=='C')[0]]
        licktR = np.array(self.lick_t)[np.where(np.array(self.licks) == 'R')[0]]
        licktL = np.array(self.lick_t)[np.where(np.array(self.licks) == 'L')[0]]

        # Pokes time and direction
        #pokesC = np.array(self.poke_t)[np.where(np.array(self.pokes)=='C')[0]]
        pokesR = np.array(self.poke_t)[np.where(np.array(self.pokes) == 'R')[0]]
        pokesL = np.array(self.poke_t)[np.where(np.array(self.pokes) == 'L')[0]]

        # rewards time and directions
        rewR = np.array(self.rew_t)[np.where(np.array(self.rew_sides) == 'R')[0]]
        rewL = np.array(self.rew_t)[np.where(np.array(self.rew_sides) == 'L')[0]]

        allGoodTrials = []
        prevHit = True
        prevAborted = False
        prevStim = 'yes'
        PrevGotReward = "yes"
        prevCorr = False
        nCorrTrialsDone = 0

        if "NCorrectionTrials" in self.parameters:
            nCorrTrials = self.parameters["NCorrectionTrials"]
        else:
            nCorrTrials = 1

        for idx,c in enumerate(self.stim_type):

            try:
                trialDuration = min(intervalDur, self.stim_t[idx+1] - self.stim_t[idx])
            except:
                trialDuration = intervalDur

            # Lick dir
            RL = licktR[np.where((licktR>=self.stim_t[idx]) * (licktR <= (self.stim_t[idx]+trialDuration)))[0]]
            LL = licktL[np.where((licktL>=self.stim_t[idx]) * (licktL <= (self.stim_t[idx]+trialDuration)))[0]]
            RP = pokesR[np.where((pokesR>=self.stim_t[idx]) * (pokesR <= (self.stim_t[idx]+trialDuration)))[0]]
            LP = pokesL[np.where((pokesL>=self.stim_t[idx]) * (pokesL <= (self.stim_t[idx]+trialDuration)))[0]]

            # rewards
            rewardR = rewR[np.where((rewR>=self.stim_t[idx]) * (rewR <= (self.stim_t[idx]+trialDuration)))[0]]
            rewardL = rewL[np.where((rewL>=self.stim_t[idx]) * (rewL <= (self.stim_t[idx]+trialDuration)))[0]]
            
            gotReward = 'none'
            if len(rewardL) > 0 and len(rewardR) > 0:
                gotReward = 'both'
            elif len(rewardL) > 0 and len(rewardR) == 0:
                gotReward = 'left'
            elif len(rewardL) == 0 and len(rewardR) > 0:
                gotReward = 'right'
            
            # Response time
            responseTime = intervalDur
            if len(RP) > 0 and len(LP) > 0:
                responseTime = min(RP[0]-self.stim_t[idx],LP[0]-self.stim_t[idx])
            elif len(RP) > 0 and len(LP) == 0:
                responseTime = RP[0] - self.stim_t[idx]
            elif len(RP) == 0 and len(LP) > 0:
                responseTime = LP[0] - self.stim_t[idx]

            # Hit/Miss
            # Take into account case where the animal does not respond at all ? hit = NaN ?
            hit = False
            if c == 0:
                if ((len(RP)>0) and (len(LP)>0) and (RP[0] > LP[0])) or ((len(LP)>0) and (len(RP)==0)):
                    hit = True
            else:
                if ((len(RP)>0) and (len(LP)>0) and (RP[0] < LP[0])) or ((len(RP)>0) and (len(LP)==0)):
                    hit = True

            #Correction trials
            # Check number of correction trials ! N=1
            corr = False
            # if self.hasCorrectionTrials:
            #     if 'Cotr' in self.stim_names[idx][0]:
            #         nCorrTrialsDone += 1
            #         corr = True
            #     else:
            #         if nCorrTrialsDone <= nCorrTrials:
            #             if not prevHit and (PrevGotReward != 'none') and not prevCorr:
            #             # if not prevHit and (PrevGotReward != 'none'):
            #                 nCorrTrialsDone += 1
            #                 corr = True
            #         else:
            #             nCorrTrialsDone = 0
            currentStim = self.stim_names[idx][0].replace('CoTr','').replace('Aborted','')

            if isinstance(self.hasCorrectionTrials,bool):
                if len(re.findall('CoTr',self.stim_names[idx][0])) > 0:
                    nCorrTrialsDone += 1
                    corr = True
            elif isinstance(self.hasCorrectionTrials,str):
                if self.hasCorrectionTrials == 'True-v1' and not prevHit and not prevAborted and prevStim == currentStim:
                        nCorrTrialsDone += 1
                        corr = True

            # if self.hasCorrectionTrials:
            #     if 'Cotr' in self.stim_names[idx][0]:
            #         nCorrTrialsDone += 1
            #         corr = True
            #     else:
            #         if not prevHit and not prevAborted and prevStim == currentStim:
            #             nCorrTrialsDone += 1
            #             corr = True

            # aborted trials
            aborted = False
            if 'Aborted' in self.stim_names[idx][0]:
                aborted = True
            elif (gotReward == 'none') and (hit) and (c != 2):
                aborted = True
            elif (gotReward == 'none') and (not hit) and (c != 2):
                # Ambiguous case where we need to differenciate aborted trials from real miss
                # Considered aborted is there is less than 1 second between trial initiation and response poke
                if responseTime < 1:
                    aborted = True
            
            trial = {'stim': c,
                'stim time': self.stim_t[idx],
                'hit': hit,
                'correction trial': corr,
                'aborted': aborted,
                'lickR': RL,
                'lickL': LL,
                'pokeR': RP,
                'pokeL': LP,
                'stimulus': self.stim_names[idx][0],
                'gotReward': gotReward,
                'trialDuration': trialDuration,
                'responseTime': responseTime,
                'session nb': self.sessionNb,
                'date': self.date,
                'parameters': self.parameters,
                'raw parameters': self.parameters_raw}

            allGoodTrials.append(trial)
            prevHit = hit
            prevAborted = aborted
            prevStim = currentStim
            # PrevGotReward = gotReward
            # prevCorr = corr

        if format is 'DF':
            allGoodTrials = pd.DataFrame(allGoodTrials)
        elif format is 'dict':
            pass
        else :
            raise ValueError('Unknown format. Possible values are "DF" or "dict".')

        return allGoodTrials



    ##### DEPRECATED ####



    def getTrials(self,intervalDur=10):
            #
            # !!! DEPRECATED !!!
            #
            # 0 - target - left
            # Lick time and direction
            #licktC = np.array(self.lick_t)[np.where(np.array(self.licks)=='C')[0]]
            licktR = np.array(self.lick_t)[np.where(np.array(self.licks)=='R')[0]]
            licktL = np.array(self.lick_t)[np.where(np.array(self.licks)=='L')[0]]

            # Pokes time and direction
            #pokesC = np.array(self.poke_t)[np.where(np.array(self.pokes)=='C')[0]]
            pokesR = np.array(self.poke_t)[np.where(np.array(self.pokes)=='R')[0]]
            pokesL = np.array(self.poke_t)[np.where(np.array(self.pokes)=='L')[0]]

            allTrials = []
            prevHit = True
            for idx,c in enumerate(self.stim_type):
                hit = False
                corr = False
                # Lick dir
                RL = licktR[np.where((licktR>=self.stim_t[idx]) * (licktR <= (self.stim_t[idx]+intervalDur)))[0]]
                LL = licktL[np.where((licktL>=self.stim_t[idx]) * (licktL <= (self.stim_t[idx]+intervalDur)))[0]]
                RP = pokesR[np.where((pokesR>=self.stim_t[idx]) * (pokesR <= (self.stim_t[idx]+intervalDur)))[0]]
                LP = pokesL[np.where((pokesL>=self.stim_t[idx]) * (pokesL <= (self.stim_t[idx]+intervalDur)))[0]]

                # Hit/Miss
                if c == 0:
                    if ((len(RP)>0) and (len(LP)>0) and (RP[0] > LP[0])) or ((len(LP)>0) and (len(RP)==0)):
                        hit = True
                else:
                    if ((len(RP)>0) and (len(LP)>0) and (RP[0] < LP[0])) or ((len(RP)>0) and (len(LP)==0)):
                        hit = True

                #Correction trials
                if not prevHit and self.hasCorrectionTrials:
                    corr = True

                trial = ['stim', c, 'stim time', self.stim_t[idx], 'hit', hit, 'correction trial', corr, 'lickR', RL, 'lickL', LL, 'pokeR', RP, 'pokeL', LP, 'stimulus', self.stim_names[idx]]
                allTrials.append(trial)
                prevHit = hit

            return allTrials

