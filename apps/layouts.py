from dash import dcc, html, dash_table

# Test session manager
import dash_ag_grid as dag

def getLayout_multipleSession(dataPath):
    layout = html.Div(
        children=[
            html.Div(
                children=[
                    # html.H1(
                    #     children="Across sessions", className="header-title"
                    # ),
                    html.P(
                        children="Analyze the behavior of the ferret across sessions.",
                        className="header-description",
                    ),
                ],
                className="header",
            ),
            html.Div(
                children=[
                    html.Div(
                        children=[
                            html.Div(children="Sessions", className="menu-title"),
                            html.Div([
                                dcc.DatePickerRange(
                                    id='datepicker',
                                    display_format='DD-MM-YYYY',
                                    first_day_of_week=1,
                                    #max_date_allowed=date.today(),
                                ),
                            html.Div(id='date-picker-range')
                            ]),
                        ],
                    ),
                    html.Div(
                        children=[
                            html.Div(children='Trial type', className='menu-title'),
                            html.Div([
                                dcc.Checklist(
                                options=[
                                    {'label': '  good trials', 'value': 'goodTrial'},
                                    {'label': '  correction trials', 'value': 'correctionTrials'},
                                    {'label': '  aborted trials', 'value': 'abortedTrials'}
                                ],
                                id='trialPicker',
                                value=['goodTrial'],
                                labelStyle={'display': 'block'},
                                className="chkbox",
                                persistence=True,
                                persistence_type="session")
                            ]),
                            html.Div(id='trial-picker')
                        ]),
                    html.Div(
                        children=[
                            html.Div(children='Time axis', className='menu-title'),
                            html.Div([
                                dcc.Dropdown(
                                options=[
                                    {'label': 'date', 'value': 'date'},
                                    {'label': 'session number', 'value': 'session number'},
                                ],
                                id='axisPicker',
                                value='session number',
                                clearable=False,
                                className = "dropdown-local",
                                persistence=True,
                                persistence_type="session"
                                ),
                            ]),
                            html.Div(id='axis-picker')
                        ]),
                    html.Div(
                        children = [
                            html.Div(children='Get the data', className='menu-title'),
                            html.Div([
                                html.Button("Download CSV", id="btn_csv",className="button"),
                                dcc.Download(id="download-dataframe-csv"),
                                ],
                                ),
                        ]),
                ],
                className='menu',
            ),
            html.Div(
                html.Div(children=[
                    html.Div(children="parameter overview", className="card-title"),
                    dash_table.DataTable(id="param-summary",
                    style_cell={'textAlign':'left','minWidth': 95, 'maxWidth': 95, 'width': 95,'font_size': '12px','whiteSpace':'normal','height':20},
                    style_data={'color': 'black', 'backgroundColor': 'white'},
                    style_table={'overflow':'scroll','height':550},
                    # sort_action="native",
                    style_data_conditional=[
                        {
                            'if': {'row_index': 'odd'},
                            'backgroundColor': 'rgb(220, 220, 220)',
                        }
                    ],
                    style_header={
                        'backgroundColor': 'rgb(210, 210, 210)',
                        'color': 'black',
                        'fontWeight': 'bold'},
                    # fill_width = False,
                    column_selectable='multi', # or 'single'
                    fixed_rows={"headers": True, 'data': 0},
                    # fixed_columns={"headers": False, "data":2}
                )],
                    className="card",
                    style = {"overflow": "scroll"}),
                className="wrapper"),
            html.Div(
                html.Div(children=[
                    html.Div(children=[
                        html.Div(children="Hit rate", className="card-title"),
                        html.Button("Download svg", id="btn-svg-1",className="button",style={"display":"none"}),
                        ],
                        className="card-header"),
                    dcc.Download(id="download-image"),
                    dcc.Graph(id='hitr-view')],
                    className='card'),
                className='wrapper'),
            html.Div(
                html.Div(children=[
                    html.Div(children=[
                        html.Div(children="Trial repartition", className="card-title"),
                        html.Button("Download svg", id="btn-svg-2",className="button",style={"display":"none"}),
                        ],
                        className="card-header"),
                    dcc.Graph(id='composition-view')],
                    className='card'),
                className='wrapper'),
            html.Div(
                html.Div(children=[
                    html.Div(children="d'", className="card-title"),
                    html.P(
                        children="For d' explanation see http://phonetics.linguistics.ucla.edu/facilities/statistics/dprime.htm",
                        className="card-description",),
                    dcc.Graph(id='dprime-view')],
                    className='card'),
                className='wrapper',), #style={'display': 'none'}
            html.Div(
                html.Div(children=[
                    html.Div(children="Hit rate per target variants", className="card-title"),
                    dcc.Graph(id='target-view')],
                    className='card'),
                className='wrapper'),
            html.Div(
                html.Div(children=[
                    html.Div(children='Percent response to the left port', className='card-title'),
                    dcc.Graph(id='left-bias-view')],
                    className='card'),
                className='wrapper'),
            html.Div(
                html.Div(children=[
                    html.Div(children='Percent of aborted trials', className='card-title'),
                    html.P(
                        children=['Percentage of initiated trials where the animal left the central port too early.',
                            html.Br(),
                            'Ideal value is below 20%.'],
                        className="card-description",),
                    dcc.Graph(id='abort-bias-view')],
                    className='card'),
                className='wrapper'),
            html.Div(
                html.Div(children=[
                    html.Div(children='Session duration', className='card-title'),
                    html.P(
                        children='Total duration of each session. The duration is calculated as the time between the start of the session and the time of the last trial initiated by the animal.',
                        className="card-description",),
                    dcc.Graph(id='session-duration-view')],
                    className='card'),
                className='wrapper'),
            html.Div(dcc.Store(id='session-dataframe')),
            html.Div(id='dummy_div'),
        ]
    )
    return layout

def getLayout_singleSession(dataPath):
    layout = html.Div(
        children=[
            # Menu bar
            html.Div(
                children=[
                    # html.H1(
                    #     children="Single session", className="header-title"
                    # ),
                    html.P(
                        children=["Ferret behavior analysis for single sessions.", html.Br(), "Specific sessions can be selected simultaneously and pooled."],
                        className="header-description",
                    ),
                ],
                className="header",
            ),
            html.Div(
                children=[
                    html.Div(
                        children=[
                            html.Div(children="Sessions", className="menu-title"),
                            html.Div([
                                dcc.Dropdown(
                                    id='session-picker',
                                    className = "dropdown-local",
                                    multi = True,
                                    searchable = True
                                ),
                            html.Div(id='session-picker-range')
                            ]),
                        ],
                    ),
                    html.Div(
                        children=[
                            html.Div(children='Trial type', className='menu-title'),
                            html.Div([
                                dcc.Checklist(
                                options=[
                                    {'label': '  good trials', 'value': 'goodTrial'},
                                    {'label': '  correction trials', 'value': 'correctionTrials'},
                                    {'label': '  aborted trials', 'value': 'abortedTrials'}
                                ],
                                id='trialPicker',
                                value=['goodTrial','correctionTrials','abortedTrials'],
                                labelStyle={'display': 'block'},
                                className="chkbox",),
                            ]),
                            html.Div(id='trial-picker')
                        ]),
                    html.Div(
                    children = [
                        html.Div(children='Get the data', className='menu-title'),
                        html.Div([
                            html.Button("Download CSV", id="btn_csv_single",className="button"),
                            dcc.Download(id="download-dataframe-single-csv"),
                            ],),
                    ]),
                ],
                className='menu',
            ),
            # / end Menu bar
            # Plots
            html.Div(
                html.Div(children=[
                    html.Div(children='Event timimg summary', className='card-title'),
                    dcc.Textarea(id='session-txt',style={'width': '100%', 'height': 300})],
                    className='card'),
                className='wrapper',
                style={'display': 'block'}
                ),
            html.Div(
                html.Div(children=[
                    html.Div(children='Session overview', className='card-title'),
                    dcc.Graph(id='session-view')],
                    className='card'),
                className='wrapper',
                style={'display': 'block'}
                ),
            html.Div(
                html.Div(children=[
                    html.Div(children="Trial overview", className="card-title"),
                    dcc.Graph(id='trial-view')],
                    className='card'),
                className='wrapper'),
            html.Div(
                html.Div(children=[
                    html.Div(children="Confusion matrix", className="card-title"),
                    dcc.Graph(id="confusion-matrix")],
                    className='card'),
                className='wrapper'),
            html.Div(
                html.Div(children=[
                    html.Div(children='Response repartition', className='card-title'),
                    html.P(
                        # children=,
                        className="card-description",
                        id='trial-hist-txt'),
                    html.Div(children=[
                        dcc.Graph(id='trial-hist'),
                        dcc.Checklist(
                            id = "probe-tickbox",
                            className="chkbox",
                            )
                            ],
                        style={"inline": "True"}
                        ),
                        ],
                    className='card'),
                className='wrapper',
                style={'display': 'block'}
                ),
            html.Div(
                html.Div(children=[
                    html.Div(children='Video of the session', className='card-title'),
                    html.Video(id = 'video-player', src='', controls=True, className='vid-pl')],
                    className='card'),
                className='wrapper',
                style={'display': 'block'}
                ),
            # / end plots
            # Storage / invisible divs
            html.Div(dcc.Store(id='this-session-dataframe')),
            html.Div(id='dummy_div1'),
        ]
    )
    return layout

# Session manager layout
def init_grid(side):
    columnDefs = [
        {'field': 'id', "checkboxSelection": True, "headerCheckboxSelection": True},
        {'field': 'color', "hide": True},
        {'field': 'date'},
        {'field': 'file session #', "sortable" : True},
        {'field': 'session #'}
    ]

    return dag.AgGrid(
        id=f'row-dragging-grid2grid-simple-{side}',
        # rowData=left_data if side == 'left' else right_data,
        columnDefs=columnDefs,
        defaultColDef={'resizable': True},
        columnSize="sizeToFit",
        persistence=True,
        dashGridOptions={
            "rowDragManaged": True,
            "rowDragEntireRow": True,
            "rowDragMultiRow": True, "rowSelection": "multiple",
            "suppressMoveWhenRowDragging": True
        },
        rowClassRules={
            "grid-green-row": 'params.data.color == "Green"',
            "grid-blue-row": 'params.data.color == "Blue"',
            "grid-red-row": 'params.data.color == "Red"',
        },
        getRowId="params.data.id",
        
    )

def getLayout_session_manager(dataPath):
    # https://dash.plotly.com/dash-ag-grid/row-dragging-external-dropzone
    layout = html.Div(
        children=[
            # Menu bar
            html.Div(
                children=[
                    # html.H1(
                    #     children="session manager", className="header-title"
                    # ),
                    html.P(
                        children="Manage which sessions to keep and which to ignore.",
                        className="header-description",
                    ),
                ],
                className="header",
            ),
            html.Div(
                children=[
                    html.Div(
                        children = [
                            html.Div(children='Apply changes to dataframe', className='menu-title'),
                            html.Button('Apply changes', id='btn-apply-changes',className="button"),
                        ]),
                    html.Div(
                        children = [
                            html.Div(children='Reset display', className='menu-title'),
                            html.Button('Reset changes', id='btn-reset-changes',className="button"),
                        ]),
                    html.Div(
                        children = [
                            html.Div(children='Save changes (edit raw csv names)', className='menu-title'),
                            html.Button('Save changes', id='btn-save-changes',className="button"),
                        ]),
                ],
                className='menu',
            ),
            html.Div(
                children=
                [
                html.Div(
                    [
                        init_grid('left'),
                        init_grid('right'),
                    ], className='row-dragging-grid-to-grid-container',
                ),
                ]
            ),
            html.Div(id='dummy_div-2'),
            dcc.Store(id="table-state-store",data = None, storage_type= "session"),
            dcc.Store(id = "animal-value-store", data = None, storage_type= "session"),
        ]
    )
    return layout