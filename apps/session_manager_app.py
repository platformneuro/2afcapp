import os
from dash import callback_context
from dash.dependencies import Output, Input
from dash.exceptions import PreventUpdate
from apps import loadBehavioralData as ld
from apps import layouts
import re
from dash import ClientsideFunction, State, clientside_callback, callback

from app import app, dataPath

# get layout
layout = layouts.getLayout_session_manager(dataPath)

# Set Grids as dropzone for each other
clientside_callback(
    ClientsideFunction('addDropZone', 'dropZoneGrid2GridComplex'),
    Output('row-dragging-grid2grid-simple-left', 'id'),
    Input('row-dragging-grid2grid-simple-right', 'id'),
    State('row-dragging-grid2grid-simple-left', 'id'),
)

@callback(
    [Output("row-dragging-grid2grid-simple-left", "rowData"),
    Output("row-dragging-grid2grid-simple-right", "rowData"),
    ],
    [State("animal-filter", "value"),
    Input("btn-reset-changes", "n_clicks"),
    Input("table-state-store",'data'),
    Input('dummy_div-2','children'),
    State("animal-value-store","data")],
    # prevent_initial_call=True,
)
def reset_rows(animalName,_,inital_list,__,previous_animal):
    # # debug
    # print("reset callback")
    # print(callback_context.triggered[0]['prop_id'])

    if inital_list is None:
        reset = True
    elif animalName != previous_animal:
        reset = True
    elif callback_context.triggered[0]['prop_id'] != 'btn-reset-changes.n_clicks':
        reset = False
    else:
        reset = True

    if reset:
        sessionList = os.listdir(os.path.join(dataPath,'Data',animalName))
        selected_sessions = [{'id': x,
                            'color': 'Green',
                            'date':  re.findall(r'_(\d+)',x),
                            'file session #': int(re.findall(r'session(\d+)_',x)[0])} for x in sessionList if '_ignore' not in x]
        ignored_sessions = [{'id': x,
                            'color': 'Red',
                            'date': re.findall(r'_(\d+)',x),
                            'file session #': int(re.findall(r'session(\d+)_',x)[0])} for x in sessionList if '_ignore' in x]

        selected_sessions = sorted(selected_sessions, key=lambda x: x['file session #'])
        ignored_sessions = sorted(ignored_sessions, key=lambda x: x['file session #'])

        for i,item in enumerate(selected_sessions):
            selected_sessions[i]['session #'] = i
        for i,item in enumerate(ignored_sessions):
            ignored_sessions[i]['session #'] = None
    else:
        selected_sessions = inital_list[0]
        ignored_sessions = inital_list[1]
    
    return selected_sessions, ignored_sessions

@callback(
    [Output("table-state-store",'data'),
     Output("animal-value-store","data"),],
    [Input("btn-apply-changes", "n_clicks"),
     State("row-dragging-grid2grid-simple-left", "virtualRowData"),
     State("row-dragging-grid2grid-simple-right", "virtualRowData"),
     Input("animal-filter", "value"),
     ],
    prevent_initial_call=True,
)

def apply_changes(_,selected_sessions,ignored_sessions,animalName):
    # # debug
    # print("apply change callback")
    # print(callback_context.triggered[0]['prop_id'])

    # Apply callback only if Apply button is pressed
    if callback_context.triggered[0]['prop_id'] == "btn-apply-changes.n_clicks":
        # Re-generate the trial dataframe with the new list of kept sessions
        # Analyze csv
        allGoodTrials = list()
        selected_sessions = sorted(selected_sessions, key=lambda x: x['file session #'])
        for files in selected_sessions:
            filePath = os.path.join(dataPath,'Data', animalName, files['id'])
            dataObj = ld.dataManage(filePath, True)
            goodTrials = dataObj.getGoodTrials()
            allGoodTrials.append(goodTrials)

        # Saving pd dataframe
        allTrialsDF = ld.trialDataFrame(allGoodTrials)

        allTrialsDF.to_pickle(os.path.join(dataPath, animalName + '_allTrialsDF'), protocol=3)
        out = [selected_sessions,ignored_sessions]
    elif callback_context.triggered[0]['prop_id'] == "animal-filter.value":
        out = None
    else:
        raise PreventUpdate # Cancel callback
    return [out, animalName]


@callback(
    Output("btn-save-changes", "style"),
    [Input("btn-save-changes", "n_clicks"),
     State("table-state-store",'data'),
     State("animal-filter", "value"),
    ],
    prevent_initial_call=True,
)

def save_changes(_,fileList,animalName):
    # # debug
    # print("save change callback - " + callback_context.triggered[0]['prop_id'])

    # Add _ignore to the raw data files (csv), removes it if necessary
    # Apply callback only if Apply button is pressed
    if callback_context.triggered[0]['prop_id'] == "btn-save-changes.n_clicks":
        rootDir = os.path.join(dataPath,'Data',animalName)
        for i,file in enumerate(fileList[0]):
            if '_ignore' in file['id']:
                os.rename(os.path.join(rootDir,file['id']),os.path.join(rootDir,file['id'].replace('_ignore','')))
        
        for i,file in enumerate(fileList[1]):
            if '_ignore' not in file['id']:
                os.rename(os.path.join(rootDir,file['id']),os.path.join(rootDir,file['id'].replace('.csv','_ignore.csv')))
    raise PreventUpdate # This callback don't need to return anything
    return {}