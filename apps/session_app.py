import os
from datetime import datetime
from dash import dcc, callback_context
from dash.dependencies import Output, Input
from dash.exceptions import PreventUpdate
import pandas as pd
from apps import fct
from apps import layouts
import re

from app import app, dataPath

# get layout
layout = layouts.getLayout_multipleSession(dataPath)

# Callbacks
# Update datepicker when new animal is selected
@app.callback(
    [Output('datepicker', 'min_date_allowed'),
    Output('datepicker', 'max_date_allowed'),
    Output('datepicker', 'start_date'),
    Output('datepicker', 'end_date'),
    # Output('animal-filter-value','data'),
    ],
    [Input("animal-filter", "value"),],
)
def update_datePicker(animalName):
    
    allTrialsDF = pd.read_pickle(os.path.join(dataPath, animalName + '_allTrialsDF'))
    minDate = allTrialsDF['date'].min()
    maxDate = allTrialsDF['date'].max()
    if type(minDate) == str:
        minDate = datetime.strptime(minDate, '%d%m%y')
        maxDate = datetime.strptime(maxDate, '%d%m%y')
    return minDate, maxDate, minDate, maxDate

# Update table display
@app.callback(
    [Output("param-summary","data"),
    Output("param-summary","columns")],
    [Input("animal-filter", "value")],
)
def update_table(animalName):
    
    # Load data for that animal
    allTrialsDF = pd.read_pickle(os.path.join(dataPath, animalName + '_allTrialsDF'))

    tableData, columns = fct.get_parameter_table(allTrialsDF)
    if len(tableData) > 0:
        tableData['date'] = tableData['date'].agg(lambda x: x.strftime('%Y-%m-%d'))
        tableData = tableData.to_dict('records')
    return tableData, columns

# To have a fancy highlight of selected column. Conflict with initial conditional colors.
# Try to solve in the future. Maybe not worth it.
# @app.callback( 
#     Output('param-summary', 'style_data_conditional'),
#     Input('param-summary', 'selected_columns')
# )
# def update_styles(selected_columns):

#     return [{
#         'if': {'row_index': 'odd'},
#         'backgroundColor': 'rgb(220, 220, 220)',
#         'if': { 'column_id': i },
#         'background_color': '#D2F3FF'
#     } for i in selected_columns]

# Update graphs
@app.callback(
    [
        Output("hitr-view", "figure"),
        Output("left-bias-view", "figure"),
        Output("abort-bias-view", "figure"),
        Output("composition-view", "figure"),
        Output("target-view", "figure"),
        Output("dprime-view", "figure"),
        Output("session-duration-view", "figure"),
        Output('session-dataframe', "data")
    ],
    [
        Input("animal-filter", "value"),
        Input("datepicker", "start_date"),
        Input("datepicker", "end_date"),
        Input("trialPicker", "value"),
        Input("axisPicker", "value"),
        Input('param-summary', 'selected_columns'),
    ],
)
def update_charts(animalName, startDate, endDate, trialOpt, XAxis, selected_column):

    # Load data for that animal
    allTrialsDF = pd.read_pickle(os.path.join(dataPath, animalName + '_allTrialsDF'))

    sessionDF = fct.getSessionDF(allTrialsDF, trialOpt, startDate, endDate)
    tableData = pd.DataFrame()
    if selected_column != None:
        # Select the data to plot the parameters (selected from the table) on the trial figure.
        try:
            tableData, columns = fct.get_parameter_table(allTrialsDF)
            sessionDF['YVal'] = 20
            sessionDF[selected_column] = tableData[selected_column]
        except:
            selected_column = None
            
    fig, fig_bias_left, fig_bias_abort, fig_comp, fig_target, fig_dprime, fig_session_duration = fct.session_plots(sessionDF, XAxis, selected_column)
    
    # Export locally a svg of the figure.
    # Figure a way to add that to a dcc.Download -> see dcc.send_file
    # https://dash.plotly.com/dash-core-components/download
    # Ideally needs a way to write the image to disk only when the download button is pressed.
    # Permission issues on server. Need to save it in cache.
    # fig.write_image("./fig1.svg")
    # fig_comp.write_image("./fig2.svg")

    return fig, fig_bias_left, fig_bias_abort, fig_comp, fig_target, fig_dprime, fig_session_duration, sessionDF.to_json(date_format='iso', orient='split')

# Download data button
@app.callback(
    [Output("download-dataframe-csv", "data"),
    Output("btn_csv", "n_clicks")],
    [Input("btn_csv", "n_clicks"),
    Input("session-dataframe","data")],
    prevent_initial_call=True,
)
def func(n_clicks,sessionDF_json):
    if n_clicks is None:
        raise PreventUpdate
    else:
        sessionDF = pd.read_json(sessionDF_json, orient='split')
        n_clicks = None
        return dcc.send_data_frame(sessionDF.to_csv, "session_dataframe.csv"), n_clicks

# Download svg buttons
@app.callback(
    [Output("download-image","data")],
    [Input("btn-svg-1","n_clicks"),
    Input("btn-svg-2","n_clicks"),],
    prevent_initial_call=True,
)
def dl_img(n_clicks1, n_clicks2):
    button_id = re.search("\d+",callback_context.triggered[0]['prop_id']).group(0)
    return [dcc.send_file("./fig{}.svg".format(button_id))]
