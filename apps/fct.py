# import os, re
# import numpy as np
# from datetime import datetime
# import math
from inspect import Parameter
#from boto import config
from matplotlib.pyplot import table
import pandas as pd
from pyparsing import alphas
from scipy.special import ndtri # Or update python to 3.8 and use statistics.NormalDist(v,mu,sigma)
from numpy import zeros
import numpy as np
import scipy.stats as stats
import plotly.express as px
import plotly.io as pio
import plotly as pty
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import yaml

#Z:\dataPi-2afc

# default figure template
pio.templates.default = "gridon"


def getSessionDF(allTrialsDF, filters, startDate, endDate):

    # Trial filters
    TrialIdx = [False] * len(allTrialsDF)
    if 'correctionTrials' in filters:
        TrialIdx = TrialIdx | (allTrialsDF['correction trial'] == True)
    if 'abortedTrials' in filters:
        TrialIdx = TrialIdx | (allTrialsDF['aborted'] == True)
    if 'goodTrial' in filters:
        TrialIdx = TrialIdx | ((allTrialsDF['correction trial'] == False) & (allTrialsDF['aborted'] == False))

    # Date filters
    dateIdx = allTrialsDF['date'].agg(lambda x: (x >= startDate) & (x <= endDate))

    # probe trials
    probeIdx = allTrialsDF['stim'] == 2
    # print(allTrialsDF['date'].max())
    
    # Calculate fraction of aborted trials
    grouped_full = allTrialsDF.groupby(['session nb corr'])
    aborted_fraction = grouped_full.apply(lambda x: (round(x['aborted'].sum() / len(x) * 100,2)))

    idx = dateIdx & TrialIdx & ~probeIdx
    allTrialsDF_filt = allTrialsDF[idx]

    grouped = allTrialsDF_filt.groupby(['session nb corr'])
    # grouped_unfilt = allTrialsDF[dateIdx].groupby(['session nb'])

    # BadTrialCount = grouped_unfilt['correction trial', 'aborted'].sum().astype(int)
    BadTrialCount = grouped[['correction trial', 'aborted']].sum().astype(int)

    leftPokes = grouped.apply(lambda x: (((x['stim'] == 0) & (x['hit'] == True)) | ((x['stim'] == 1) & (x['hit'] == False))).sum())

    # missCount = grouped['hit'].agg(lambda x: (x == False).sum()).reset_index().drop(columns='session nb corr')
    # missCount = missCount.rename(columns={"hit": "miss"})
    # hitCount = grouped['hit'].agg(lambda x: (x == True).sum()).reset_index().drop(columns='session nb corr')
    missCount = grouped['hit'].agg(lambda x: (x == False).sum())
    hitCount = grouped['hit'].agg(lambda x: (x == True).sum())

    session_duration = grouped['stim time'].max() / 60

    # Aborted correction trials count as aborted
    missCountAborted = grouped.apply(lambda x: pd.Series((x['hit'] == False) & (x['aborted'] == True)).sum())
    hitCountAborted = grouped.apply(lambda x: pd.Series((x['hit'] == True) & (x['aborted'] == True)).sum())

    missCountCT = grouped.apply(lambda x: pd.Series((x['hit'] == False) & (x['correction trial'] == True) & (x['aborted'] == False)).sum())
    hitCountCT = grouped.apply(lambda x: pd.Series((x['hit'] == True) & (x['correction trial'] == True) & (x['aborted']==False)).sum())

    hitCountT = grouped.apply(lambda x: pd.Series((x['stim'] == 0) & (x['hit'] == True)).sum())
    hitCountNT = grouped.apply(lambda x: pd.Series((x['stim'] == 1) & (x['hit'] == True)).sum())
    missCountT = grouped.apply(lambda x: pd.Series((x['stim'] == 0) & (x['hit'] == False)).sum())
    missCountNT = grouped.apply(lambda x: pd.Series((x['stim'] == 1) & (x['hit'] == False)).sum())

    dates = grouped['date'].first()

    ### Probe trials ###
    # TO DO
    # idxP = dateIdx & TrialIdx & probeIdx
    # print(sum(probeIdx))
    # allTrialsDF_probes = allTrialsDF[idxP]
    # print(allTrialsDF_probes)
    # grouped_probes = allTrialsDF_probes.groupby(['session nb corr'])

    # missCountProbes = grouped_probes['hit'].agg(lambda x: (x == False).sum())
    # hitCountProbes = grouped_probes['hit'].agg(lambda x: (x == True).sum())

    # missCountAbortedProbes = grouped_probes.apply(lambda x: pd.Series((x['hit'] == False) & (x['aborted'] == True)).sum())
    # hitCountAbortedProbes = grouped_probes.apply(lambda x: pd.Series((x['hit'] == True) & (x['aborted'] == True)).sum())

    # missCountCTProbes = grouped_probes.apply(lambda x: pd.Series((x['hit'] == False) & (x['correction trial'] == True)).sum())
    # hitCountCTProbes = grouped_probes.apply(lambda x: pd.Series((x['hit'] == True) & (x['correction trial'] == True)).sum())

    # sessionDF = pd.concat([hitCount, missCount, hitCountT, hitCountNT, missCountT, missCountNT, BadTrialCount, leftPokes, dates, hitCountAborted, missCountAborted, hitCountCT, missCountCT, hitCountProbes, missCountProbes, hitCountAbortedProbes, missCountAbortedProbes, hitCountCTProbes, missCountCTProbes], axis=1)
    # sessionDF_probes = pd.concat([ hitCountProbes, missCountProbes, hitCountAbortedProbes, missCountAbortedProbes, hitCountCTProbes, missCountCTProbes], axis=1)
    # print(sessionDF.tail(10))
    # sessionDF.columns = ['hit', 'miss', 'hitTarget', 'hitNonTarget', 'missTarget', 'missNonTarget', 'correction trial', 'aborted', 'NLeftPokes', 'date', 'hitCountAborted', 'missCountAborted', 'hitCountCT', 'missCountCT', 'hitCountProbes', 'missCountProbes', 'hitCountAbortedProbes', 'missCountAbortedProbes', 'hitCountCTProbes', 'missCountCTProbes']
    
    ### / probe trials ###

    sessionDF = pd.concat([hitCount, missCount, hitCountT, hitCountNT, missCountT, missCountNT, BadTrialCount, leftPokes, dates, hitCountAborted, missCountAborted, hitCountCT, missCountCT, aborted_fraction, session_duration], axis=1)
    # sessionDF = sessionDF.rename(columns={'hit': 'hit', 'miss': 'miss', 0: 'hitTarget', 1: 'hitNonTarget', 2: 'missTarget', 3: 'missNonTarget', 4: 'NLeftPokes', 5: 'date'})
    sessionDF.columns = ['hit', 'miss', 'hitTarget', 'hitNonTarget', 'missTarget', 'missNonTarget', 'correction trial', 'aborted', 'NLeftPokes', 'date', 'hitCountAborted', 'missCountAborted', 'hitCountCT', 'missCountCT', 'abortedF', 'session duration']

    sessionDF['hitNoBad'] = sessionDF['hit'] - sessionDF['hitCountAborted'] - sessionDF['hitCountCT']
    sessionDF['missNoBad'] = sessionDF['miss'] - sessionDF['missCountAborted'] - sessionDF['missCountCT']
    # sessionDF['hitProbeNoBad'] = sessionDF['hitCountProbes'] - sessionDF['hitCountAbortedProbes'] - sessionDF['hitCountCTProbes']
    # sessionDF['missProbeNoBad'] = sessionDF['missCountProbes'] - sessionDF['missCountAbortedProbes'] - sessionDF['missCountCTProbes']
    sessionDF['NTrials'] = sessionDF['hit'] + sessionDF['miss']
    # sessionDF['hitRate'] = round(sessionDF['hit'] / sessionDF['NTrials'] * 100, 2)
    sessionDF['hitRateTarget'] = round(sessionDF['hitTarget'] / (sessionDF['hitTarget'] + sessionDF['missTarget']) * 100, 2)
    sessionDF['hitRateNonTarget'] = round(sessionDF['hitNonTarget'] / (sessionDF['hitNonTarget'] + sessionDF['missNonTarget']) * 100, 2)
    sessionDF['hitRate'] = round((sessionDF['hitTarget'] + sessionDF['hitNonTarget']) / (sessionDF['hitNonTarget'] + sessionDF['missNonTarget'] + sessionDF['hitTarget'] + sessionDF['missTarget']) * 100, 2)
    sessionDF['session number'] = sessionDF.index.values
    # sessionDF.set_index(dates, inplace=True)

    sessionDF['d prime'] = ndtri(sessionDF['hitRateTarget']/100) - ndtri(1-(sessionDF['hitRateNonTarget']/100))
    # sessionDF['d prime NT'] = ndtri(sessionDF['hitRateNonTarget']/100) - ndtri(1-(sessionDF['hitRateTarget']/100))
    sessionDF['bias left'] = round(sessionDF['NLeftPokes'] / sessionDF['NTrials'] * 100, 2)

    # print(sessionDF['date'].head(10))
 
    # Separate targets
    groupedStim = allTrialsDF_filt.groupby(['session nb corr', 'stimulus'])
    a = groupedStim['hit'].value_counts()
    tList = a.index.get_level_values('stimulus').unique().tolist()
    # tList = [x.replace('Aborted','').replace('CoTr','') for x in tList]
    # catName = ('','CoTr','Aborted','CoTrAborted')
    for targ in tList:
        b=0
        c=0
        # for cat in catName:
        try:
            a_filt = a.xs(targ, level='stimulus')
        except:
            continue
        try:
            b = b + a_filt.xs(True, level='hit')
        except: # Rare case where no hit on this target happend across the sessions considered
            b = b + pd.Series(data=[0]*len(a_filt), index=a_filt.index.get_level_values('session nb corr'))
        try:
            c = c + a_filt.xs(False, level='hit')
        except: # Rare case where no hit on this target happend across the sessions considered
            c = c + pd.Series(data=[0]*len(a_filt), index=a_filt.index.get_level_values('session nb corr'))
        d = pd.concat([b,c], axis=1).fillna(0)

        d.columns = ['n_' + targ +'hit','n_' + targ + 'miss']
        d['t_' + targ] = d['n_' + targ +'hit'] / (d['n_' + targ +'hit'] + d['n_' + targ +'miss']) * 100
        sessionDF = pd.concat([sessionDF, d], axis=1)

    # colList = [i for i in tList if i not in excludeList and 'CoTr' not in i]
    sessionDF['pooled_targ_hit'] = sessionDF[['n_' + i + 'hit' for i in tList]].sum(axis=1)
    sessionDF['pooled_targ_miss'] = sessionDF[['n_' + i + 'miss' for i in tList]].sum(axis=1)
    sessionDF['pooled_targ'] = sessionDF['pooled_targ_hit'] / (sessionDF['pooled_targ_hit'] + sessionDF['pooled_targ_miss']) * 100
    # print(sessionDF.tail(3))

    ### Old version - specific to Gouda / Bouton
    # # sessionDF['pooledHit'] = 0 # DIRTY - TO REMOVE
    # # sessionDF['pooledMiss'] = 0 # DIRTY - TO REMOVE
    # # print(sessionDF['pooledMiss'].tail(3))
    # groupedStim = allTrialsDF_filt.groupby(['session nb corr', 'stimulus'])
    # a = groupedStim['hit'].value_counts()
    # stimNames = a.index.get_level_values('stimulus').unique().tolist()
    # tList = [i for i in stimNames if 'solicit_' in i]
    # for targ in tList:
    #     a_filt = a.xs(targ, level='stimulus')
    #     try:
    #         b = a_filt.xs(True, level='hit')
    #     except: # Rare case where no hit on this target happend across the sessions considered
    #         b = pd.Series(data=[0]*len(a_filt), index=a_filt.index.get_level_values('session nb corr'))
    #     try:
    #         c = a_filt.xs(False, level='hit')
    #     except: # Rare case where no hit on this target happend across the sessions considered
    #         c = pd.Series(data=[0]*len(a_filt), index=a_filt.index.get_level_values('session nb corr'))
    #     d = pd.concat([b,c], axis=1).fillna(0)
    #     # d.columns = ['hit','miss']
    #     # d['t_' + targ] = d['hit'] / (d['hit'] + d['miss']) * 100
    #     # sessionDF = pd.concat([sessionDF, d['t_' + targ]], axis=1)

    #     d.columns = ['n_' + targ +'hit','n_' + targ + 'miss']
    #     d['t_' + targ] = d['n_' + targ +'hit'] / (d['n_' + targ +'hit'] + d['n_' + targ +'miss']) * 100
    #     sessionDF = pd.concat([sessionDF, d], axis=1)

    #     # if targ != "t_solicit_005" or targ != "t_solicit_012" or targ != "t_solicit_003":
    #     #     d.fillna(0, inplace=True)
    #     #     print(d.tail(3))
    #     #     sessionDF['pooledHit'] = sessionDF['pooledHit'] + d['hit'] # DIRTY - TO REMOVE
    #     #     sessionDF['pooledMiss'] = sessionDF['pooledMiss'] + d['miss'] # DIRTY - TO REMOVE
    # excludeList = ('solicit_005','solicit_003', 'solicit_012')
    # colList = [i for i in tList if i not in excludeList and 'CoTr' not in i]
    # sessionDF['pooled_targ_hit'] = sessionDF[['n_' + i + 'hit' for i in colList]].sum(axis=1)
    # sessionDF['pooled_targ_miss'] = sessionDF[['n_' + i + 'miss' for i in colList]].sum(axis=1)
    # sessionDF['pooled_targ'] = sessionDF['pooled_targ_hit'] / (sessionDF['pooled_targ_hit'] + sessionDF['pooled_targ_miss']) * 100
    # # print(sessionDF.tail(3))
    ### - end of old version

    return sessionDF

def session_plots(sessionDF, XAxis, selected_column):

    # config = {
    # 'toImageButtonOptions': {
    #     'format': 'svg', # one of png, svg, jpeg, webp
    #     'filename': 'custom_image',
    #     'height': 500,
    #     'width': 700,
    #     'scale': 1 # Multiply title/legend/axis/canvas sizes by this factor
    # }
    # }

    # Main plot hit rate
    fig = px.line(sessionDF, x=XAxis, y=['hitRate', 'hitRateTarget', 'hitRateNonTarget'], labels={'value': r'hit rate (%)'}, hover_data={'session number':False, 'variable': False})
    # if selected_column != None:
    #     for i in range(0,len(selected_column)):
    #         sessionDF.dropna(axis = 0, subset = [selected_column[i]], inplace=True) # Remove nans from the database
    #         figB = px.bar(sessionDF, x = XAxis, y = 'YVal', color = selected_column[i], hover_data={'session number':False, 'YVal':False})
    #     # to hide colorbar : showscale=False
    #         fig = go.Figure(data = fig.data + figB.data, layout=fig.layout)
    #     # fig['data'][2]['hovertemplate']='Line 2 Name<br>x=%{x}<br>y=%{y}<extra></extra>'
    if selected_column == None:
        # Handle bug from Dash:multiselect - returned selected columns is None initially, and [] if all columns have been unselected.
        selected_column = []
    if len(selected_column) > 0:
        # to hide colorbar : showscale=False
        sessionDF.dropna(axis = 0, subset = selected_column, inplace=True)
        sessionDF['selected parameters'] = sessionDF[selected_column].astype(str).agg('-'.join, axis=1)
        figB = px.bar(sessionDF, x = XAxis, y = 'YVal', color = 'selected parameters', hover_data={'session number':False, 'YVal':False})
        fig = go.Figure(data = fig.data + figB.data, layout=fig.layout)


    fig.add_hline(y=50, line_width=2, line_dash="dash", line_color="dimgray")
    fig.add_hline(y=20, line_width=2, line_dash="dash", line_color="gray")
    fig.add_hline(y=80, line_width=2, line_dash="dash", line_color="gray")
    fig.update_layout(hovermode="x unified",yaxis_range=[0,100])
    # fig.update_traces(hovertemplate='%{x} % <br>') #
    # fig.update_xaxes(showspikes=True)
    # fig.update_yaxes(showspikes=True)

    # Plot hit rate per targets
    tList = [i for i in sessionDF.columns if 't_' in i]
    displayNames = [x.replace('t_','') for x in tList]
    fig_target = pty.plot(sessionDF, x=XAxis, kind='line', y=tList, labels={'value': r'hit rate (%)'})
    fig_target.add_hline(y=50, line_width=2, line_dash="dash", line_color="dimgray")
    fig_target.add_hline(y=20, line_width=2, line_dash="dash", line_color="gray")
    fig_target.add_hline(y=80, line_width=2, line_dash="dash", line_color="gray")
    # print(len(fig.data))
    for idx, name in enumerate(displayNames):
        fig_target.data[idx].name = name
        fig_target.data[idx].hovertemplate = name
    fig_target.update_layout(yaxis_range=[0,100])

    # Bias to left
    fig_bias_left = px.line(sessionDF, x=XAxis, y=["bias left"], labels={'value': r'response left (%)'})
    fig_bias_left.update_layout(yaxis_range=[0,100], showlegend=False)
    
    # fig_bias = make_subplots(specs=[[{"secondary_y": True}]])
    # fig_bias.add_trace(go.Scatter(sessionDF, x = XAxis, y=["bias left"], mode='line', labels={'value': r'% response left'}), secondary_y=False,)
    
    # fig_bias.add_trace(go.Scatter(sessionDF, x = XAxis, y=['abortedF'], mode='line', labels={'value': r'% aborted trials'}), secondary_y=True,)
    # fig_bias.add_trace(go.Scatter(x = sessionDF[XAxis], y=sessionDF["bias left"], mode='lines', name = 'left bias'), secondary_y=False,)
    # fig_bias.add_trace(go.Scatter(x = sessionDF[XAxis], y=sessionDF["abortedF"], mode='lines', name = 'aborted trials'), secondary_y=True,)
    fig_bias_left.add_hline(y=50, line_width=2, line_dash="dash", line_color="gray")
    fig_bias_left.update_layout(yaxis_range=[0,100], showlegend=False)
    # fig_bias.update_xaxes(title_text=XAxis)
    # fig_bias.update_yaxes(title_text=r"% of responses to the left", secondary_y=False)
    # fig_bias.update_yaxes(title_text=r"% of aborted trials", secondary_y=True)

    # Percent aborted
    fig_bias_abort = px.line(sessionDF, x=XAxis, y=["abortedF"], labels={'value': r'aborted trials (%)'})
    fig_bias_abort.update_layout(yaxis_range=[0,100], showlegend=False)

    # fig_bias = make_subplots(specs=[[{"secondary_y": True}]])
    # # fig_bias.add_trace(go.Scatter(sessionDF, x = XAxis, y=["bias left"], mode='line', labels={'value': r'% response left'}), secondary_y=False,)
    
    # # fig_bias.add_trace(go.Scatter(sessionDF, x = XAxis, y=['abortedF'], mode='line', labels={'value': r'% aborted trials'}), secondary_y=True,)
    # fig_bias.add_trace(go.Scatter(x = sessionDF[XAxis], y=sessionDF["bias left"], mode='lines', name = 'left bias'), secondary_y=False,)
    # fig_bias.add_trace(go.Scatter(x = sessionDF[XAxis], y=sessionDF["abortedF"], mode='lines', name = 'aborted trials'), secondary_y=True,)
    # fig_bias.add_hline(y=50)
    # fig_bias.update_xaxes(title_text=XAxis)
    # fig_bias.update_yaxes(title_text=r"% of responses to the left", secondary_y=False)
    # fig_bias.update_yaxes(title_text=r"% of aborted trials", secondary_y=True)

    # Trial repartition
    # fig_comp = px.bar(sessionDF, x=XAxis, y=["hitNoBad", "missNoBad", 'correction trial', 'aborted'], labels={"value": "N trials"})
    # # fig_comp = px.bar(sessionDF, x=XAxis, y=["hitNoBad", "missNoBad",'hitCountAborted', 'missCountAborted', 'hitCountCT', 'missCountCT', 'hitCountProbes', 'missCountProbes', 'hitCountAbortedProbes', 'missCountAbortedProbes'], labels={"value": "N trials"})
    # newnames = {"hitNoBad": 'hit', "missNoBad": "miss", 'correction trial': 'correction trial', 'aborted': 'aborted'}
    # fig_comp.for_each_trace(lambda t: t.update(name = newnames[t.name],
    #                                     legendgroup = newnames[t.name],
    #                                     hovertemplate = t.hovertemplate.replace(t.name, newnames[t.name])
    #                                     )
    #                 )

    # Trial repartition v2
    # sessionDF_reshaped = sessionDF[['hitNoBad', 'missNoBad','hitCountAborted', 'missCountAborted', 'hitCountCT', 'missCountCT', XAxis]]
    # sessionDF_reshaped = pd.melt(sessionDF_reshaped, id_vars = XAxis)
    # sessionDF_reshaped['outcome'] = sessionDF_reshaped['variable'].str.replace('NoBad','')
    # sessionDF_reshaped['outcome'] = sessionDF_reshaped['outcome'].str.replace('CountCT','')
    # sessionDF_reshaped['outcome'] = sessionDF_reshaped['outcome'].str.replace('CountAborted','')
    # sessionDF_reshaped['trial type'] = sessionDF_reshaped['variable'].str.replace('hit','')
    # sessionDF_reshaped['trial type'] = sessionDF_reshaped['trial type'].str.replace('miss','')

    # print(sessionDF_reshaped.head(10))
    # fig_comp = px.bar(sessionDF_reshaped, 
    #     x = XAxis, y = 'value', color = 'trial type', pattern_shape = 'outcome',
    #     pattern_shape_sequence=[".", "x"],
    #     labels={'value': 'number of trials', 'NoBad': 'true trials'})
    # fig_comp.update_traces(marker=dict(line_color="grey", pattern_fillmode="replace"))
    #color_discrete_sequence = ["red", "red", "green" ,"green", "blue","blue"], 'rgba(255, 255, 255, 0)'

    sessionDF_reshaped = sessionDF[['hitNoBad', 'missNoBad','hitCountAborted', 'missCountAborted', 'hitCountCT', 'missCountCT', XAxis]]
    # print(sessionDF_reshaped.keys())
    sessionDF_reshaped.rename(columns={"hitNoBad": "real trials, hit", "missNoBad": "real trials, miss",
        "hitCountAborted": "aborted trials, hit", "missCountAborted": "aborted trials, miss",
        "hitCountCT": "correction trials, hit", "missCountCT": "correction trials, miss", XAxis: XAxis}, errors="raise", inplace = True)
    # print(sessionDF_reshaped.keys())
    sessionDF_reshaped = pd.melt(sessionDF_reshaped, id_vars = XAxis)
    colors = ['rgba(154, 51, 19, 1)','rgba(154, 51, 19, 0.5)','rgba(43,76,111, 1)','rgba(43,76,111, 0.5)','rgba(170, 160, 57, 1)','rgba(170, 160, 57, 0.5)']
    fig_comp = px.bar(sessionDF_reshaped, 
        x = XAxis, y = 'value', color = 'variable',
        color_discrete_sequence = colors,
        labels={'value': 'number of trials', 'variable': 'Trial type'})
    fig_comp.update_traces(marker=dict(line_color="grey", pattern_fillmode="replace"))

    # D prime
    # fig_dprime = pty.plot(sessionDF, x=XAxis, kind='line', y=['d prime'])
    fig_dprime = px.line(sessionDF, x=XAxis, y=['d prime'],labels={'value': 'D prime'})
    fig_dprime.update_layout(showlegend=False)

    # Session duration
    fig_session_duration = px.line(sessionDF, x=XAxis, y=['session duration'],labels={'value': 'time (minutes)'})
    fig_session_duration.update_layout(yaxis_range=[0, sessionDF['session duration'].max()], showlegend=False)


    fig = setLayout(fig)
    fig_bias_left = setLayout(fig_bias_left)
    fig_bias_abort = setLayout(fig_bias_abort)
    fig_comp = setLayout(fig_comp, ignore_legend=True)
    fig_target = setLayout(fig_target)
    fig_dprime = setLayout(fig_dprime)
    fig_session_duration = setLayout(fig_session_duration)

    # fig.show(config=config)
    # fig_dprime.show(config=config)
    # fig_target.show(config=config)

    return fig, fig_bias_left, fig_bias_abort, fig_comp, fig_target, fig_dprime, fig_session_duration

def get_parameter_table(allTrialsDF):
    trialsDF = allTrialsDF
    # param_table = pd.DataFrame()
    param_table = list()
    columns = list()
    if 'raw parameters' in trialsDF.keys():
        if len(trialsDF['raw parameters'][0]) > 0:
            trialsDF = allTrialsDF[['date','session nb corr','raw parameters']]
            trialsDF.drop_duplicates(subset=["session nb corr"], inplace=True)  
        elif len(trialsDF['parameters'])>0:
            trialsDF = allTrialsDF[['date','session nb corr','parameters']]
            trialsDF.drop_duplicates(subset=["session nb corr"], inplace=True)

        for i in range(0,len(trialsDF)):
                # param_table = param_table.append(pd.DataFrame.from_dict([trialsDF.iloc[i,1]]))
                dat = dict()
                dat['date'] = trialsDF.iloc[i]['date']
                dat['session'] = trialsDF.iloc[i]['session nb corr']
                dat.update(trialsDF.iloc[i]['raw parameters'])
                # if isinstance(dat,dict):
                dat = {k: str(dat[k]) if isinstance(dat[k],(dict, list)) else dat[k] for k in dat.keys()}
                param_table.append(dat)

        columns = [{"name": i, "id": i, "hideable": True, "selectable": True} for i in param_table[-1].keys()]
        param_table = pd.DataFrame(param_table)
        param_table.set_index('session',inplace=True)
        param_table['session'] = param_table.index
    return param_table, columns

def get_trial_fig(allTrialsDF, sessionNB, filters):

    # Get trial for current session
    # session filters
    if isinstance(sessionNB,int):
        sessionNB = [sessionNB]
    sessionIdx = allTrialsDF['session nb corr'].agg(lambda x: (x in sessionNB))
    trialsDF = allTrialsDF[sessionIdx]                                        

    # trialsDF = allTrialsDF[(allTrialsDF['session nb corr'] == sessionNB)]

    # Apply trial filters
    TrialIdx = [False] * len(trialsDF)
    if 'correctionTrials' in filters:
        TrialIdx = TrialIdx | (trialsDF['correction trial'] == True)
    if 'abortedTrials' in filters:
        TrialIdx = TrialIdx | (trialsDF['aborted'] == True)
    if 'goodTrial' in filters:
        TrialIdx = TrialIdx | ((trialsDF['correction trial'] == False) & (trialsDF['aborted'] == False))

    trialsDF['trial nb'] = trialsDF.index
    trialsDF = trialsDF[TrialIdx]
    
    # Get poke times
    trialsDF['poke left'] = trialsDF['pokeL'] - trialsDF['stim time']
    trialsDF['poke right'] = trialsDF['pokeR'] - trialsDF['stim time']
    trialsDF['trial index left'] = trialsDF.apply(lambda x: len(x['pokeL']) * [x['trial nb']],axis=1)
    trialsDF['trial index right'] = trialsDF.apply(lambda x: len(x['pokeR']) * [x['trial nb']],axis=1)
    trialsDF['stimulus name left'] = trialsDF.apply(lambda x: len(x['pokeL']) * [x['stimulus']],axis=1)
    trialsDF['stimulus name right'] = trialsDF.apply(lambda x: len(x['pokeR']) * [x['stimulus']],axis=1)
    timesDFL = pd.DataFrame()
    timesDFL['trial'] =  trialsDF['trial index left'].explode()
    timesDFL['stimulus name'] =  trialsDF['stimulus name left'].explode()
    timesDFL['poke left'] = trialsDF['poke left'].explode()
    timesDFR = pd.DataFrame()
    timesDFR['trial'] =  trialsDF['trial index right'].explode()
    timesDFR['stimulus name'] =  trialsDF['stimulus name right'].explode()
    timesDFR['poke right'] = trialsDF['poke right'].explode()
    timesDF = pd.concat([timesDFL,timesDFR])

    # Session text
    snd_times = trialsDF['stim time'].to_numpy()
    PL_times = np.hstack(trialsDF['pokeL'].to_numpy())
    PR_times = np.hstack(trialsDF['pokeR'].to_numpy())
    snd_txt = trialsDF['stimulus'].to_numpy()
    allTimes = np.concatenate((snd_times,PL_times,PR_times))
    allNames = np.concatenate((snd_txt,['poke left'] * len(PL_times),['poke right'] * len(PR_times)))
    sortIdx = np.argsort(allTimes)
    allTimes.sort()
    allNames = allNames[sortIdx]
    session_txt = "No parameters saved."
    if 'raw parameters' in trialsDF.keys():
        if len(trialsDF['raw parameters'][0]) > 0:
            # session_txt = str(trialsDF['raw parameters'][0]).replace("'","").replace(',','\n')
            session_txt = yaml.dump(trialsDF['raw parameters'][0], Dumper = yaml.Dumper)
        elif len(trialsDF['parameters'])>0:
            session_txt = yaml.dump(trialsDF['parameters'][0], Dumper = yaml.Dumper)
            
    session_txt = 'Parameters:\n' + session_txt + '\n---\n\nEvents:\n' + '\n'.join(str(d) + ' - ' + c for d,c in zip(allTimes, allNames))

    # Trial by trial view
    cols = ['red', 'green','blue' ]
    #trialsDF['trialColor'] = ['red' if x == 0 else 'green' for x in trialsDF['stim']]
    trialsDF['trialColor'] = [cols[int(i)] for i in trialsDF['stim']]

    deltaT = trialsDF['stim time'].diff()
    deltaT[0] = 10
    deltaT[deltaT > 10] = 10
    # trialsDF[:]['deltaT'] = a.copy(deep=False)

    fig1 = px.scatter(timesDF,x=['poke left','poke right'],y='trial',hover_data=['stimulus name'],labels={'value': 'time (second)'})
    fig1.layout.update(shapes=
    [dict(type="rect",
        x0=0, x1=deltaT[k], y0=k-0.5, y1=k+0.5,
        line_color="black", line_width=0, fillcolor=tr['trialColor'],opacity=0.5) for k,tr in trialsDF.iterrows()]
    )

    # View over time
    a = pd.DataFrame()
    a['Time'] = trialsDF['lickL'].explode()
    a['r'] = 1
    a['Event'] = 'lick left'
    b = pd.DataFrame()
    b['Time'] = trialsDF['lickR'].explode()
    b['r'] = 2
    b['Event'] = 'lick right'
    c = pd.DataFrame()
    c['Time'] = trialsDF['pokeL'].explode()
    c['r'] = 4
    c['Event'] = 'poke left'
    d = pd.DataFrame()
    d['Time'] = trialsDF['pokeR'].explode()
    d['r'] = 5
    d['Event'] = 'poke right'
    e = pd.DataFrame()
    e['Time'] = trialsDF['stim time'].explode()
    e['r'] = 7
    e['Event'] = 'sound presentation'
    allv = pd.concat([a,b,c,d,e])
    allv['marker'] = 'line-ns'
    fig2 = px.scatter(allv,x='Time',y='Event',color='Event',labels={'Time': 'time (second)', 'Event': 'type of event'})
    fig2.update_traces(marker = {'symbol':'line-ns-open', 'size': 20})
    fig2.layout.update(showlegend=False)

    # Histogram view
    grouped = trialsDF.groupby(['stim', 'aborted'])
    a = grouped['hit'].value_counts()
    
    f_obs = zeros((3,2))
    for i in range(3):
        # print(a)
        try:
            f_obs[i][0] = a.loc[(i,False,True)]
        except:
            pass
        try:
            f_obs[i][1] = a.loc[(i,False,False)]
        except:
            pass

    # print(f_obs)
    # names = list()
    # stims = list()
    # for n in a.index:
    #     s = f"{stimNames[int(n[0])]} - {'aborted' if n[1] else ''} - {'hit' if n[2] else 'miss'}"
    #     stims.append(n[0])
    #     names.append(s)
    # flat = pd.concat([flat, pd.Series(names), pd.Series(stims)],axis=1)
    # flat.columns=['hit','names','stim']
    # chi2, pval = stats.chisquare(f_obs, f_exp=None, ddof=0, axis=1)

    fDF = pd.DataFrame(f_obs,columns=('hit','miss'), index=('left','right','probe'))
    # fDF['p value'] = pval
    # print(pval)
    # titlestr = 'left p={0:.3f} - right p={1:.3f} - probe p={2:.3f}'.format(*fDF['p value'].values)
    # fDF['p value'] = round(fDF['p value'],3)
    # fig3 = px.bar(fDF, y = ['hit', 'miss'], barmode='group',labels={'index': 'stimulus direction', 'value': 'trial'})

    # Confusion matrix and MI calculation
    confusionMat = f_obs[0:2:1, 0:2:1]
    confusionMat[1] = np.flip(confusionMat[1])
    miVal = calcMI(np.array(confusionMat))
    title = '{:2.2f}% of maximal mutual information'.format(miVal[1])
    fig4 = px.imshow(np.round((confusionMat / confusionMat.sum(axis=0)) * 100, decimals = 2), zmin=0, zmax=100, color_continuous_scale = 'gray_r', x = ['left','right'], y = ['left', 'right'], labels = dict(x="stimulus", y="response", color=r"% response"), aspect='equal', title = title)
    # fig4 = go.Figure(go.Heatmap(np.round((confusionMat / confusionMat.sum(axis=0)) * 100, decimals = 2), text = confusionMat, zmin=0, zmax=100, color_continuous_scale = 'gray_r', x = ['left','right'], y = ['left', 'right'], labels = dict(x="stimulus", y="response", color=r"% response"), title = title))

    # colormap list : https://plotly.com/python/colorscales/
    #text_auto=True, aspect="auto"
    # plotly.express.imshow(img, zmin=None, zmax=None, origin=None, labels={}, x=None, y=None, animation_frame=None, facet_col=None, facet_col_wrap=None, facet_col_spacing=None, facet_row_spacing=None, color_continuous_scale=None, color_continuous_midpoint=None, range_color=None, title=None, template=None, width=None, height=None, aspect=None, contrast_rescaling=None, binary_string=None, binary_backend='auto', binary_compression_level=4, binary_format='png', text_auto=False) → plotly.graph_objects._figure.Figure)
    # 

    # flat = a.reset_index(drop=True)
    # names = list()
    # stims = list()
    # for n in a.index:
    #     s = f"{'hit' if n[1] else 'miss'}"
    #     stims.append(stimNames[int(n[0])])
    #     names.append(s)
    # flat = pd.concat([flat, pd.Series(names), pd.Series(stims)],axis=1)
    # flat.columns=['hit','names','stim']
    # fig3 = px.bar(flat,x='stim',y='hit',color='names', barmode = 'group', labels={'hit': 'N trials', 'stim': 'target', 'names': 'trial outcome'})

    fig1 = setLayout(fig1)
    fig2 = setLayout(fig2)
    # fig3 = setLayout(fig3)

    # return fig1, fig2, fig3, fig4, trialsDF, fDF, session_txt
    return fig1, fig2, fig4, trialsDF, session_txt

def get_barChart_fig(allTrialsDF, sessionNB, filters, tickValues):

    # Get trial for current session
    # session filters
    if isinstance(sessionNB,int):
        sessionNB = [sessionNB]
    sessionIdx = allTrialsDF['session nb corr'].agg(lambda x: (x in sessionNB))
    trialsDF = allTrialsDF[sessionIdx]                                        

    # trialsDF = allTrialsDF[(allTrialsDF['session nb corr'] == sessionNB)]
    # Apply trial filters
    TrialIdx = [False] * len(trialsDF)
    if 'correctionTrials' in filters:
        TrialIdx = TrialIdx | (trialsDF['correction trial'] == True)
    if 'abortedTrials' in filters:
        TrialIdx = TrialIdx | (trialsDF['aborted'] == True)
    if 'goodTrial' in filters:
        TrialIdx = TrialIdx | ((trialsDF['correction trial'] == False) & (trialsDF['aborted'] == False))

    trialsDF['trial nb'] = trialsDF.index
    trialsDF = trialsDF[TrialIdx]

    # Filter the selected probe stimuli
    trialsDF = trialsDF[(trialsDF["stim"] != 2) | (trialsDF["stimulus"].isin(tickValues))]

    # Histogram view
    grouped = trialsDF.groupby(['stim', 'aborted'])
    a = grouped['hit'].value_counts()
    
    f_obs = zeros((3,2))
    for i in range(3):
        # print(a)
        try:
            f_obs[i][0] = a.loc[(i,False,True)]
        except:
            pass
        try:
            f_obs[i][1] = a.loc[(i,False,False)]
        except:
            pass

    chi2, pval = stats.chisquare(f_obs, f_exp=None, ddof=0, axis=1)

    fDF = pd.DataFrame(f_obs,columns=('hit','miss'), index=('left','right','probe'))
    fDF['p value'] = pval
    fig3 = px.bar(fDF, y = ['hit', 'miss'], barmode='group',labels={'index': 'stimulus direction', 'value': 'trial'})
    return fig3, fDF

def filter_trialDF(allTrialsDF, sessionNB, filters):
    
    # Get trial for current session
    # session filters
    if isinstance(sessionNB,int):
        sessionNB = [sessionNB]
    sessionIdx = allTrialsDF['session nb corr'].agg(lambda x: (x in sessionNB))
    trialsDF = allTrialsDF[sessionIdx]                                        

    # trialsDF = allTrialsDF[(allTrialsDF['session nb corr'] == sessionNB)]
    # Apply trial filters
    TrialIdx = [False] * len(trialsDF)
    if 'correctionTrials' in filters:
        TrialIdx = TrialIdx | (trialsDF['correction trial'] == True)
    if 'abortedTrials' in filters:
        TrialIdx = TrialIdx | (trialsDF['aborted'] == True)
    if 'goodTrial' in filters:
        TrialIdx = TrialIdx | ((trialsDF['correction trial'] == False) & (trialsDF['aborted'] == False))

    trialsDF['trial nb'] = trialsDF.index
    trialsDF = trialsDF[TrialIdx]
    return trialsDF

def setLayout(fig, ignore_legend=False):
    font_family = 'Arial'
    if not ignore_legend:
        legends = dict(
            orientation="h",
            yanchor="bottom",
            y=1.02,
            xanchor="right",
            x=1
        )
        fig.update_layout(legend=legends)
    fig.update_layout(font_family=font_family, font_color='dimgray', yaxis=dict(tickfont=dict(size=18)), xaxis=dict(tickfont=dict(size=18)))
    # fig.update_layout(font=dict(family=font_family, color='dimgray',size=30))
    fig.update_xaxes(title_font=dict(size=20, family=font_family, color='dimgray'),linecolor='dimgray',linewidth=1)
    fig.update_yaxes(title_font=dict(size=20, family=font_family, color='dimgray'),linecolor='dimgray',linewidth=1)
    return fig

def calcMI(respMat):
    # respMat: matrix of trial repartition
    # return mutual information and mutual information as % of maximal available information
    # see https://en.wikipedia.org/wiki/Mutual_information
    eps = np.finfo(float).eps # Bug for log2(0)
    nRow = respMat.shape[0]
    nCol = respMat.shape[1]
    sumX = respMat.sum(axis=0)
    sumY = respMat.sum(axis=1)
    sumX = sumX.flatten()
    sumY = sumY.flatten()
    nTot = respMat.sum()
    maxMI = np.log2(nRow)
    MI = 0
    for x in range(nRow):
        for y in range(nCol):
            pxy = respMat[x, y]/nTot
            px = sumY[x]/sumY.sum()
            py = sumX[y]/sumX.sum()
            # py = sumY[y]/sumY.sum()
            # px = sumX[x]/sumX.sum()
            MI = MI + (pxy * np.log2(eps + pxy / (px * py)))
    MI_percent = MI / maxMI * 100
    return MI, MI_percent


# def empty_figure():
#     fig = px.scatter(x=[],y=[])
#     return fig

# def ndtri(y0):

#     s2pi = 2.50662827463100050242E0

#     P0 = [
#         -5.99633501014107895267E1,
#         9.80010754185999661536E1,
#         -5.66762857469070293439E1,
#         1.39312609387279679503E1,
#         -1.23916583867381258016E0,
#     ]

#     Q0 = [
#         1,
#         1.95448858338141759834E0,
#         4.67627912898881538453E0,
#         8.63602421390890590575E1,
#         -2.25462687854119370527E2,
#         2.00260212380060660359E2,
#         -8.20372256168333339912E1,
#         1.59056225126211695515E1,
#         -1.18331621121330003142E0,
#     ]

#     P1 = [
#         4.05544892305962419923E0,
#         3.15251094599893866154E1,
#         5.71628192246421288162E1,
#         4.40805073893200834700E1,
#         1.46849561928858024014E1,
#         2.18663306850790267539E0,
#         -1.40256079171354495875E-1,
#         -3.50424626827848203418E-2,
#         -8.57456785154685413611E-4,
#     ]

#     Q1 = [
#         1,
#         1.57799883256466749731E1,
#         4.53907635128879210584E1,
#         4.13172038254672030440E1,
#         1.50425385692907503408E1,
#         2.50464946208309415979E0,
#         -1.42182922854787788574E-1,
#         -3.80806407691578277194E-2,
#         -9.33259480895457427372E-4,
#     ]

#     P2 = [
#         3.23774891776946035970E0,
#         6.91522889068984211695E0,
#         3.93881025292474443415E0,
#         1.33303460815807542389E0,
#         2.01485389549179081538E-1,
#         1.23716634817820021358E-2,
#         3.01581553508235416007E-4,
#         2.65806974686737550832E-6,
#         6.23974539184983293730E-9,
#     ]

#     Q2 = [
#         1,
#         6.02427039364742014255E0,
#         3.67983563856160859403E0,
#         1.37702099489081330271E0,
#         2.16236993594496635890E-1,
#         1.34204006088543189037E-2,
#         3.28014464682127739104E-4,
#         2.89247864745380683936E-6,
#         6.79019408009981274425E-9,
#     ]

#     if y0 <= 0 or y0 >= 1:
#         raise ValueError("ndtri(x) needs 0 < x < 1")
#     negate = True
#     y = y0
#     if y > 1.0 - 0.13533528323661269189:
#         y = 1.0 - y
#         negate = False

#     if y > 0.13533528323661269189:
#         y = y - 0.5
#         y2 = y * y
#         x = y + y * (y2 * polevl(y2, P0) / polevl(y2, Q0))
#         x = x * s2pi
#         return x

#     x = math.sqrt(-2.0 * math.log(y))
#     x0 = x - math.log(x) / x

#     z = 1.0 / x
#     if x < 8.0:
#         x1 = z * polevl(z, P1) / polevl(z, Q1)
#     else:
#         x1 = z * polevl(z, P2) / polevl(z, Q2)
#     x = x0 - x1
#     if negate:
#         x = -x
#     return x

# def polevl(x, coef):
#     accum = 0
#     for c in coef:
#         accum = x * accum + c
#     return accum

# def getFileNames(dataPath):
#     fileList = (os.listdir(dataPath))
#     # remove ignored files (merged leftover)
#     fileList = [f for f in fileList if len(re.findall("_ignore", f)) == 0]
#     sessionNB = list()
#     sessionDate = list()
#     sessionSplit = list()
#     for files in fileList:
#         sNB = int(re.findall('session([0-9]{1,3})', files)[0])
#         sessionNB.append(sNB)
#         sessionDate.append(datetime.strptime(re.findall('_([0-9]{6})', files)[0], '%d%m%y'))
#         sessionSplit.append(sessionDate[-1].strftime('%d%m%y') + '_' + str(sessionNB[-1]))
#     #fileList = fileList(np.argsort(sessionNB))
#     fileList[:] = [fileList[i] for i in np.argsort(sessionNB)]
#     sessionDate[:] = [sessionDate[i] for i in np.argsort(sessionNB)]
#     sessionSplit[:] = [sessionSplit[i] for i in np.argsort(sessionNB)]
#     sessionNB.sort()
#     return fileList, sessionDate, sessionNB, sessionSplit
