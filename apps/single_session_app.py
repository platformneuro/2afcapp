import os
import dash
from dash import dcc, callback_context
from dash import html
from dash.dependencies import Output, Input
from dash.exceptions import PreventUpdate
import pandas as pd
from apps import fct, layouts
from app import app, dataPath

# To DO:
# - Check MI calculation, does not seem correct.

# Get page layout 
layout = layouts.getLayout_singleSession(dataPath)

# Update session picker when new animal is selected
@app.callback(
    [Output('session-picker', 'options'),
    Output('session-picker', 'value'),
    ],
    [Input("animal-filter", "value")],
)

def update_sessionPicker(animalName):
    allTrialsDF = pd.read_pickle(os.path.join(dataPath, animalName + '_allTrialsDF'))
    allSessionNb = allTrialsDF.groupby(['date','session nb corr']).size().reset_index().drop(0, axis=1)
    allSessionNb['option'] = allSessionNb.apply(lambda x: {'label':  str(x['session nb corr']) + ' - ' + str(x['date'])[:10], 'value': x['session nb corr']}, axis=1)
    options = allSessionNb['option'].tolist()
    value = allTrialsDF['session nb corr'].iloc[-1]
    return options, value

# Update graphs
@app.callback(
    [Output("trial-view", "figure"),
    Output("session-view", "figure"),
    Output("confusion-matrix", "figure"),
    Output('this-session-dataframe', "data"),
    Output("session-txt", "value")],
    [
        Input("animal-filter", "value"),
        Input("session-picker", "value"),
        Input("trialPicker", "value"),
    ],
)
def update_charts(animalName, sessionNB, trialOpt):

    # Load data for that animal
    allTrialsDF = pd.read_pickle(os.path.join(dataPath, animalName + '_allTrialsDF'))

    # session overview plot
    fig_trial, fig_session, fig_CM , trialsDF, session_txt = fct.get_trial_fig(allTrialsDF, sessionNB, trialOpt)

    return fig_trial, fig_session, fig_CM, trialsDF.to_json(date_format='iso', orient='split'), session_txt


# Update bar charts
@app.callback(
    [Output("trial-hist", "figure"),
    Output("trial-hist-txt", "children"),],
    [
        Input("animal-filter", "value"),
        Input("session-picker", "value"),
        Input("trialPicker", "value"),
        Input("probe-tickbox", "value"),
    ],
)
def update_barCharts(animalName, sessionNB, trialOpt, tickValues):

    # Load data for that animal
    allTrialsDF = pd.read_pickle(os.path.join(dataPath, animalName + '_allTrialsDF'))

    # histogram of hit / miss trials
    fig_trial_hist, fDF = fct.get_barChart_fig(allTrialsDF, sessionNB, trialOpt, tickValues)
    txt = ["For probe trials, hit/miss corresponds to responses to right/left respectively", html.Br(), \
        "Statistical significance (chi square test): left p={0:.3f} - right p={1:.3f} - probe p={2:.3f}".format(*fDF['p value'].values), \
            html.Br(), "Select probe stimuli to include in the bar plot from the list."]

    return fig_trial_hist, txt

# Update the tick box for the probe stimuli
@app.callback(
    [Output("probe-tickbox", "value"),
    Output("probe-tickbox", "options")],
    [Input("animal-filter", "value"),
    Input("session-picker", "value"),
    Input("trialPicker", "value"),
    ],
)
def get_probe_stim_names(animalName, sessionNB, trialOpt):
    # Load data for that animal
    allTrialsDF = pd.read_pickle(os.path.join(dataPath, animalName + '_allTrialsDF'))

    # get trial filtered dataframe
    allTrialsDF_filt = fct.filter_trialDF(allTrialsDF, sessionNB, trialOpt)
    probe_list = allTrialsDF_filt[allTrialsDF_filt["stim"]==2]["stimulus"].unique()
    probe_list.sort()
    options = [{"label": x, "value": x} for x in probe_list]

    return probe_list, options

# Update video stream
@app.callback(
    [Output('video-player','src')],
    [Input("session-picker", "value"),
    Input("animal-filter", "value")]
)
def load_video(sessionNb, animalName):
    allTrialsDF = pd.read_pickle(os.path.join(dataPath, animalName + '_allTrialsDF'))
    date = pd.DatetimeIndex(allTrialsDF[allTrialsDF['session nb corr']==sessionNb]['date'].unique()).strftime('%d%m%y')
    sessionNbFull = allTrialsDF[allTrialsDF['session nb corr']==sessionNb]['session nb'].unique()

    if len(date)>0:
        p = '/videos/{}/{}_session{}_{}.mp4'.format(animalName,animalName,sessionNbFull[0],date[0])
    else:
        p = ''
    return [p]

# Download button
@app.callback(
    [Output("download-dataframe-single-csv", "data"),
    Output("btn_csv_single", "n_clicks")],
    [Input("btn_csv_single", "n_clicks"),
    Input("this-session-dataframe","data")],
    prevent_initial_call=True,
)
def func(n_clicks,sessionDF_json):
    if n_clicks is None:
        raise PreventUpdate
    else:
        sessionDF = pd.read_json(sessionDF_json, orient='split')
        n_clicks = None
        return dcc.send_data_frame(sessionDF.to_csv, "session_dataframe.csv"), n_clicks

