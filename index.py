from dash.exceptions import PreventUpdate
from dash import dcc
from dash import html, callback_context
from dash.dependencies import Input, Output, State
import dash_bootstrap_components as dbc
from app import app, dataPath
from app import server
# import all pages in the app
from apps import session_app, single_session_app, session_manager_app
import argparse
import os


# building the navigation bar
# https://github.com/facultyai/dash-bootstrap-components/blob/master/examples/advanced-component-usage/Navbars.py
dropdown = dbc.DropdownMenu(
    children=[
        dbc.DropdownMenuItem("across session analysis", href="/across_session"),
        dbc.DropdownMenuItem("single session analyses", href="/single_session"),
        dbc.DropdownMenuItem("session manager", href="/session_manager"),
        # dbc.DropdownMenuItem("multi session analyses", href="/multi_session"),
    ],
    nav = True,
    in_navbar = True,
    label = "Explore your data",
    id = 'navbar-menu',
)

navbar = dbc.Navbar(
    dbc.Container(
        [
            html.A(
                # Use row and col to control vertical alignment
                dbc.Row(
                    [
                        dbc.Col(dbc.NavbarBrand("Ferret 2AFC analysis", className="ml-2")),
                    ],
                    align="center",
                ),
                href="/",
            ),
            dbc.Col(
                dcc.Dropdown(
                    id="animal-filter",
                    options=[
                        {"label": animalName.replace('_allTrialsDF', ''), "value": animalName.replace('_allTrialsDF', '')}
                        for animalName in sorted(os.listdir(dataPath)) if '_allTrialsDF' in animalName
                    ],
                    # value = "Gouda",
                    placeholder="Pick a ferret",
                    clearable = False,
                    className = "dropdown-local",
                    persistence = True,
                    persistence_type = "session",
                    style={"float":"right"}
                    )
                ),
                dbc.NavbarToggler(id="navbar-toggler2"),
                dbc.Col(
                    dbc.Collapse(
                        dbc.Nav(
                            # right align dropdown menu with ml-auto className
                            [dropdown], className="ml-auto", navbar=True
                        ),
                    id="navbar-collapse2",
                    navbar=True,
                    ),
                ),
        ]
    ),
    color="dark",
    dark=True,
    className="mb-4",
)

def toggle_navbar_collapse(n, is_open):
    if n:
        return not is_open
    return is_open

app.callback(
    Output("navbar-collapse2","is_open"),
    [Input("navbar-toggler2","n_clicks")],
    [State("navbar-collapse2", "is_open")],
)(toggle_navbar_collapse)

# embedding the navigation bar
app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    navbar,
    html.Div(id='page-content'),
])


@app.callback([Output('page-content', 'children'),
               Output('navbar-menu','label')],
              [Input('url', 'pathname')])

def display_page(pathname):
    if pathname == '/across_session':
        # return session_app
        layout = session_app.layout
        display_name = 'across session'
        # return session_app
    elif pathname == '/single_session':
        # return single_session_app.layout
        layout = single_session_app.layout
        display_name = 'single session'
    elif pathname == '/session_manager':
        layout = session_manager_app.layout
        display_name = 'session manager'
    else:
        raise PreventUpdate
    return [layout, display_name]

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Dash server application for 2AFC analysis.')
    parser.add_argument('-host', dest='host', default='127.0.0.1',type=str, help='host ip (default: 127.0.0.1)')
    parser.add_argument('-port', dest='port', default='8050',type=int, help='port number (default: 8050)')
    parser.add_argument('-debug', dest='debug', action="store_true", help='debug server (default: False)')
    args = parser.parse_args()

    app.run_server(host=args.host,port=args.port,debug=args.debug)
